package com.company;
import java.io.IOException;
import java.util.Scanner;

public class Main {
    final static double phi = 3.14;
    public static void main(String[] args){
        // write your code here
        System.out.println(pasir());
        System.out.println("Kalkulator penghitung luas dan volum");
        System.out.println(pasir());
        menuUtama();
    }
    public static void menuUtama(){
        System.out.println("1.Hitung luas bidang");
        System.out.println("2.Hitung volum");
        System.out.println("0.Tutup aplikasi");
        System.out.print("pilih menu : ");
        Scanner in = new Scanner(System.in);
        int pilihmenu = in.nextInt();
        switch (pilihmenu) {
            case 1 :
                menuBidang();

                break;
            case 2 :
                menuVolum();

                break;
            default:
                System.exit(0);
        }
    }
    public static void menuBidang(){
        System.out.println("1.persegi");
        System.out.println("2.lingkaran");
        System.out.println("3.segitiga");
        System.out.println("4.persegi panjang");
        System.out.println("0.kembali ke menu sebelumnya");
        System.out.print("pilih menu : ");
        Scanner in = new Scanner(System.in);
        int pilihmenu = in.nextInt();
        switch (pilihmenu) {
            case 1 :
                menuHitung("persegi");

                break;
            case 2 :
                menuHitung("lingkaran");

                break;
            case 3 :
                menuHitung("segitiga");

                break;
            case 4 :
                menuHitung("persegi panjang");

                break;
            case 0 :
                menuUtama();

                break;
            default:
                System.exit(0);
        }
    }
    public static void menuVolum() {
        System.out.println("1.kubus");
        System.out.println("2.balok");
        System.out.println("3.tabung");
        System.out.println("0.kembali ke menu sebelumnya");
        System.out.print("pilih menu : ");
        Scanner in = new Scanner(System.in);
        int pilihmenu = in.nextInt();
        switch (pilihmenu) {
            case 1 :
                menuHitung("kubus");

                break;
            case 2 :
                menuHitung("balok");

                break;
            case 3 :
                menuHitung("tabung");

                break;
            case 0 :
                menuUtama();

                break;
            default:
                System.exit(0);
        }

    }

    public static void menuHitung(String bidang) {
        System.out.println(pasir());
        System.out.println("Anda Memilih "+ bidang);
        System.out.println(pasir());

        switch (bidang) {
            case "persegi":
                System.out.print("Masukan sisi: ");
                Scanner in = new Scanner(System.in);
                int sisi = in.nextInt();
                System.out.println("Processing .....");
                String Hasil = persegi(sisi, bidang);
                System.out.println(Hasil);
                break;
            case "lingkaran":
                System.out.print("Masukan jari-jari: ");
                Scanner in2 = new Scanner(System.in);
                double jari_jari = in2.nextInt();
                System.out.println("Processing .....");
                String Hasil2 = lingkaran(jari_jari, bidang);
                System.out.println(Hasil2);
                break;
            case "segitiga":
                System.out.print("Masukan alas : ");
                Scanner in3 = new Scanner(System.in);
                int alas = in3.nextInt();
                System.out.print("Masukan tinggi : ");
                Scanner in4 = new Scanner(System.in);
                int tinggi = in4.nextInt();
                System.out.println("Processing .....");
                String Hasil3 = segitiga(alas, tinggi, bidang);
                System.out.println(Hasil3);
                break;
            case "persegi panjang":
                System.out.print("Masukan panjang: ");
                Scanner in5 = new Scanner(System.in);
                int panjang = in5.nextInt();
                System.out.print("Masukan lebar: ");
                Scanner in6 = new Scanner(System.in);
                int lebar = in6.nextInt();
                System.out.println("Processing .....");
                String Hasil4 = persegiPanjang(panjang, lebar, bidang);
                System.out.println(Hasil4);
                break;

            //menuhitungvolum

            case "kubus":
                System.out.print("Masukan sisi: ");
                Scanner in7 = new Scanner(System.in);
                int sisi_kubus = in7.nextInt();
                System.out.println("Processing .....");
                String Hasil5 = kubus(sisi_kubus, bidang);
                System.out.println(Hasil5);
                break;
            case "balok":
                System.out.print("Masukan panjang: ");
                Scanner in8 = new Scanner(System.in);
                int panjang_balok = in8.nextInt();
                System.out.print("Masukan lebar: ");
                Scanner in9 = new Scanner(System.in);
                int lebar_balok = in9.nextInt();
                System.out.print("Masukan tinggi: ");
                Scanner in10 = new Scanner(System.in);
                int tinggi_balok = in10.nextInt();
                System.out.println("Processing .....");
                String Hasil6 = balok(panjang_balok, lebar_balok, tinggi_balok, bidang);
                System.out.println(Hasil6);
                break;
            case "tabung":
                System.out.print("Masukan tinggi: ");
                Scanner in11 = new Scanner(System.in);
                int tinggi_tabung = in11.nextInt();
                System.out.print("Masukan jari-jari: ");
                Scanner in12 = new Scanner(System.in);
                int jari_jari_tabung = in12.nextInt();
                System.out.println("Processing .....");
                String Hasil7 = tabung(tinggi_tabung, jari_jari_tabung, bidang);
                System.out.println(Hasil7);
                break;

        }
        System.out.println(pasir());
        //   System.out.println("tekan apa saja untuk kembali ke menu utama");
        pressAnyKeyToContinue();
    }

    public static String persegi(int sisi, String bidang) {
        int hasil  = sisi*sisi;
        String Hsl="luas dari "+bidang+" adalah "+ hasil;
        return Hsl;
    }
    public static String persegiPanjang(int panjang, int lebar, String bidang) {
        int hasil  = panjang*lebar;
        String Hsl="luas dari "+bidang+" adalah "+ hasil;
        return Hsl;
    }
    public static String segitiga(int alas, int tinggi, String bidang ){
        double f_alas = (double) alas;
        double f_tinggi = (double) tinggi;
        double hasil   = 0.5 * f_alas * f_tinggi;
        String Hsl="luas dari "+bidang+" adalah "+ hasil;
        return Hsl;
    }

    public static String lingkaran(double jari_jari, String bidang) {
        double hasil = (phi * jari_jari * jari_jari);
        String Hsl="luas dari "+bidang+" adalah "+ hasil;
        return Hsl;
    }
    public static String kubus (int sisi, String bidang){
        int hasil  = sisi*sisi*sisi;
        String Hsl="luas dari "+bidang+" adalah "+ hasil;
        return Hsl;
    }
    public static String balok (int panjang, int lebar , int tinggi, String bidang) {
        int hasil  = panjang*lebar*tinggi;
        String Hsl="luas dari "+bidang+" adalah "+ hasil;
        return Hsl;
    }
    public static String tabung (double tinggi , double jari_jari, String bidang){
        double hasil  = phi * tinggi * jari_jari * jari_jari;
        String Hsl="luas dari "+bidang+" adalah "+ hasil;
        return Hsl;
    }


    public static String pasir(){
        String nyah = "";
        for (int i=0; i<50; i++){
            nyah += "-";
        }
        return nyah;
    }
    public static void  pressAnyKeyToContinue()
    {
        System.out.println("tekan enter untuk ke menu utama...");
        try
        {
            System.in.read();
            menuUtama();
        }
        catch(Exception e)
        {

        }
    }
}

